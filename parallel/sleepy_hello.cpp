#include <pthread.h>
#include <unistd.h>
#include <iostream>

void *thread(void *args) {
  sleep(1);
  std::cout << "Sleepy world" << std::endl;
  return nullptr;
}

int main() {
  pthread_t tid;
  pthread_create(&tid, nullptr, thread, nullptr);
  pthread_join(tid, nullptr);
  return 0;
}
