#include <pthread.h>
#include <iostream>

void *thread(void *args) {
  std::cout << "Hello, World" << std::endl;
  return nullptr;
}

int main() {
  pthread_t tid;
  pthread_create(&tid, nullptr, thread, nullptr);
  pthread_join(tid, nullptr);
  return 0;
}
