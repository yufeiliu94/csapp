#include <netdb.h>
#include <iostream>
#include <sstream>

std::string format_string_array(char **xs) {
  std::ostringstream os;
  for (auto i = 0; xs != nullptr and xs[i] != nullptr; ++i) {
    os << "\"" << xs[i] << "\", ";
  }
  return os.str();
}

int main(int argc, char *argv[]) {
  if (argc != 2) {
    std::cerr << "Usage: gethostbyname [HOSTNAME]" << std::endl;
    exit(-1);
  }
  const auto host = gethostbyname(argv[argc - 1]);
  if (host == nullptr) {
    std::cerr << "Unknown host: " << argv[argc - 1] << std::endl;
    exit(-1);
  }
  // clang-format off
  std::cout << "struct hostent {\n"
            << "  char  *h_name       = \"" << host->h_name << "\";\n"
            << "  char **h_aliases    = {" << format_string_array(host->h_aliases) << "};\n"
            << "  int    h_addrtype   = AF_INET" << (host->h_addrtype == AF_INET6 ? "6" : "") << ";\n"
            << "  int    h_length     = " << host->h_length << ";\n"
            << "  char **h_addr_list  = {" << format_string_array(host->h_addr_list) << "};\n"
            << "}" << std::endl;
  // clang-format on
  return 0;
}
