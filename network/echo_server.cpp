#include <arpa/inet.h>
#include <netdb.h>
#include <unistd.h>
#include <algorithm>
#include <cstring>
#include <iostream>
#include "csapp.hpp"
#include "csapp/rio.hpp"
#include "csapp/socket.hpp"

constexpr size_t buffer_size = 1024;

bool echo(int fd) {
  ssize_t n, zero = 0;
  char buffer[buffer_size];
  csapp::rio::rio_t rio(fd);
  while ((n = rio.readline(buffer, buffer_size)) != 0) {
    std::clog << "[ECHO Server] Received " << n << " bytes" << std::endl;
    if (strncmp(buffer, "BYE", 3) == 0) {
      return true;
    }
    csapp::rio::writen(fd, buffer, (size_t)std::max(zero, n));
  }
  return false;
}

int main(int argc, char *argv[]) {
  if (argc != 2) {
    std::cerr << "Usage echo_server [PORT]" << std::endl;
    exit(-1);
  }
  const auto port = atoi(argv[1]);
  const auto listen_fd = csapp::socket::open_listenfd((uint16_t)port);
  std::clog << "[ECHO Server] Listening on port " << port << std::endl;
  while (true) {
    sockaddr_in client{};
    socklen_t client_size = sizeof(client);
    const auto connection =
        accept(listen_fd, (sockaddr *)(&client), &client_size);
    auto host =
        gethostbyaddr(&client.sin_addr, sizeof(client.sin_addr), AF_INET);
    auto addr = inet_ntoa(client.sin_addr);
    std::clog << "[ECHO Server] Connected to " << host->h_name << "(" << addr
              << ")" << std::endl;
    const auto shutdown = echo(connection);
    close(connection);
    if (shutdown) {
      std::clog << "[ECHO Server] Bye." << std::endl;
      break;
    }
  }
  return 0;
}
