#include <unistd.h>
#include <cstring>
#include <iostream>
#include "csapp.hpp"
#include "csapp/rio.hpp"
#include "csapp/socket.hpp"

using csapp::rio::rio_t;

constexpr size_t buffer_size = 1024;

int main(int argc, char *argv[]) {
  if (argc != 3) {
    std::cerr << "Usage: echo_client [HOST] [PORT]" << std::endl;
    exit(-1);
  }
  const auto host = argv[1];
  const auto port = atoi(argv[2]);
  const auto client = csapp::socket::open_clientfd(host, (uint16_t)port);
  rio_t rio(client);

  char buffer[buffer_size];

  while (fgets(buffer, buffer_size, stdin) != nullptr) {
    csapp::rio::writen(client, buffer, strlen(buffer));
    if (rio.readline(buffer, buffer_size) <= 0) {
      std::clog << "[ECHO Client] Server closed." << std::endl;
      break;
    }
    std::cout << "[ECHO Client]: " << buffer << std::endl;
  }
  close(client);
  return 0;
}
