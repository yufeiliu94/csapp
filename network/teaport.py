#!/usr/bin/python3.6
# encoding: UTF-8

"""
Teaport 是一个易于理解的 HTTP 服务器
它参考 CSAPP 中的 TINY 实现
"""

import socket
import enum
import abc
import re
import io
import os
import collections
import functools
import sys
import logging
import chardet
import mimetypes
import typing

"""`
要求 Python 3.6, 并假装 Python 4 不会那么早到来
"""
if sys.version_info[0] != 3 or sys.version_info[1] < 6:
    raise RuntimeError('Python >= 3.6.x required')

"""
常量
"""

NEWLINE_BYTES = b'\r\n'
UTF8 = 'UTF-8'
UTF8_BYTES = b'UTF-8'


class SocketIO:
    """
    针对 Socket 的 Robust I/O 函数 (CSAPP Chapter 10)
    """

    @staticmethod
    def read_from(source: socket.socket, buffer_size: int = 4096) -> io.BytesIO:
        bio = io.BytesIO()
        while True:
            seg = source.recv(buffer_size)
            if seg is None:
                break
            bio.write(seg)
            if len(seg) < buffer_size:
                break
        bio.seek(0, 0)
        return bio

    @staticmethod
    def write_to(dest: socket.socket, data: bytes) -> int:
        return dest.sendall(data)


class TeaportError(Exception):
    def __init__(self, what):
        super().__init__(self, what)

    @abc.abstractmethod
    def to_response(self) -> 'Response':
        pass


class ToBytes:
    @abc.abstractmethod
    def to_bytes(self) -> bytes:
        pass


class FromBytes:
    @classmethod
    @abc.abstractclassmethod
    def from_bytes(cls, value: bytes):
        pass


class LiteralEnum(FromBytes, ToBytes, enum.Enum):
    @classmethod
    def from_bytes(cls, value: bytes):
        for item in cls:
            if item.value == value:
                return item
        else:
            raise ValueError('Unknown value {!r}, acceptable values: {}'
                             .format(value, ', '.join(repr(e.value) for e in cls)))

    def to_bytes(self) -> bytes:
        return self.value


class RequestMethod(LiteralEnum):
    GET, POST, PUT, DELETE = b'GET', b'POST', b'PUT', b'DELETE'
    CONNECT, HEAD, OPTIONS, PATCH = b'CONNECT', b'HEAD', b'OPTIONS', b'PATCH'


class HTTPVersion(LiteralEnum):
    HTTP_1_0, HTTP_1_1, HTTP_2_0 = b'HTTP/1.0', b'HTTP/1.1', b'HTTP/2.0'


class HeaderItem(LiteralEnum):
    Accept = b'Accept'
    AcceptCharset = b'Accept-Charset'
    AcceptEncoding = b'Accept-Encoding'
    AcceptLanguage = b'Accept-Language'
    AcceptRanges = b'Accept-Ranges'
    AccessControlAllowCredentials = b'Access-Control-Allow-Credentials'
    AccessControlAllowHeaders = b'Access-Control-Allow-Headers'
    AccessControlAllowMethods = b'Access-Control-Allow-Methods'
    AccessControlAllowOrigin = b'Access-Control-Allow-Origin'
    AccessControlExposeHeaders = b'Access-Control-Expose-Headers'
    AccessControlMaxAge = b'Access-Control-Max-Age'
    AccessControlRequestHeaders = b'Access-Control-Request-Headers'
    AccessControlRequestMethod = b'Access-Control-Request-Method'
    Age = b'Age'
    Allow = b'Allow'
    Authorization = b'Authorization'
    CacheControl = b'Cache-Control'
    Connection = b'Connection'
    ContentDisposition = b'Content-Disposition'
    ContentEncoding = b'Content-Encoding'
    ContentLanguage = b'Content-Language'
    ContentLength = b'Content-Length'
    ContentLocation = b'Content-Location'
    ContentRange = b'Content-Range'
    ContentSecurityPolicy = b'Content-Security-Policy'
    ContentSecurityPolicyReportOnly = b'Content-Security-Policy-Report-Only'
    ContentType = b'Content-Type'
    Cookie = b'Cookie'
    Cookie2 = b'Cookie2'
    DNT = b'DNT'
    Date = b'Date'
    ETag = b'ETag'
    Expect = b'Expect'
    Expires = b'Expires'
    Forwarded = b'Forwarded'
    From = b'From'
    Host = b'Host'
    IfMatch = b'If-Match'
    IfModifiedSince = b'If-Modified-Since'
    IfNoneMatch = b'If-None-Match'
    IfRange = b'If-Range'
    IfUnmodifiedSince = b'If-Unmodified-Since'
    KeepAlive = b'Keep-Alive'
    LargeAllocation = b'Large-Allocation'
    LastModified = b'Last-Modified'
    Location = b'Location'
    Origin = b'Origin'
    Pragma = b'Pragma'
    ProxyAuthenticate = b'Proxy-Authenticate'
    ProxyAuthorization = b'Proxy-Authorization'
    PublicKeyPins = b'Public-Key-Pins'
    PublicKeyPinsReportOnly = b'Public-Key-Pins-Report-Only'
    Range = b'Range'
    Referer = b'Referer'
    ReferrerPolicy = b'Referrer-Policy'
    RetryAfter = b'Retry-After'
    Server = b'Server'
    SetCookie = b'Set-Cookie'
    SetCookie2 = b'Set-Cookie2'
    SourceMap = b'SourceMap'
    StrictTransportSecurity = b'Strict-Transport-Security'
    TE = b'TE'
    TimingAllowOrigin = b'Timing-Allow-Origin'
    Tk = b'Tk'
    Trailer = b'Trailer'
    TransferEncoding = b'Transfer-Encoding'
    UpgradeInsecureRequests = b'Upgrade-Insecure-Requests'
    UserAgent = b'User-Agent'
    Vary = b'Vary'
    Via = b'Via'
    WWWAuthenticate = b'WWW-Authenticate'
    Warning = b'Warning'
    XContentTypeOptions = b'X-Content-Type-Options'
    XDNSPrefetchControl = b'X-DNS-Prefetch-Control'
    XForwardedFor = b'X-Forwarded-For'
    XForwardedHost = b'X-Forwarded-Host'
    XForwardedProto = b'X-Forwarded-Proto'
    XFrameOptions = b'X-Frame-Options'
    XXSSProtection = b'X-XSS-Protection'


class ResponseStatusCode(ToBytes):
    __CODE__ = {
        100: b'Continue',
        101: b'Switching Protocols',
        200: b'OK',
        201: b'Created',
        202: b'Accepted',
        203: b'Non-Authoritative Information',
        204: b'No Content',
        205: b'Reset Content',
        206: b'Partial Content',
        300: b'Multiple Choices',
        301: b'Moved Permanently',
        302: b'Found',
        303: b'See Other',
        304: b'Not Modified',
        307: b'Temporary Redirect',
        308: b'Permanent Redirect',
        400: b'Bad Request',
        401: b'Unauthorized',
        403: b'Forbidden',
        404: b'Not Found',
        405: b'Method Not Allowed',
        406: b'Not Acceptable',
        407: b'Proxy Authentication Required',
        408: b'Request Timeout',
        409: b'Conflict',
        410: b'Gone',
        411: b'Length Required',
        412: b'Precondition Failed',
        413: b'Payload Too Large',
        414: b'URI Too Long',
        415: b'Unsupported Media Type',
        416: b'Range Not Satisfiable',
        417: b'Expectation Failed',
        426: b'Upgrade Required',
        428: b'Precondition Required',
        429: b'Too Many Requests',
        431: b'Request Header Fields Too Large',
        451: b'Unavailable For Legal Reasons',
        500: b'Internal Server Error',
        501: b'Not Implemented',
        502: b'Bad Gateway',
        503: b'Service Unavailable',
        504: b'Gateway Timeout',
        505: b'HTTP Version Not Supported',
        511: b'Network Authentication Required'
    }
    __UNKNOWN__ = b'UNKNOWN CODE'
    __slots__ = ['code']

    def __init__(self, code: int):
        self.code = code

    def __str__(self):
        return f'<{self.code}: {self.__CODE__.get(self.code, self.__UNKNOWN__).decode()}>'

    def __repr__(self):
        return f'Response({self.code})'

    def to_bytes(self) -> bytes:
        return b' '.join((str(self.code).encode(), self.__CODE__[self.code]))


class Header(ToBytes):
    def __init__(self, header: dict):
        self.header = header

    @staticmethod
    def from_header(items: [bytes]):
        return Header(dict(item.split(b': ') for item in items))

    @staticmethod
    def from_content(mime: str, length: int, encoding: str = None):
        k = HeaderItem
        header = {}
        if isinstance(length, int):
            header[k.ContentLength] = str(length).encode(encoding=encoding or 'UTF-8')
        if isinstance(mime, str):
            header[k.ContentType] = mime.encode(encoding=encoding or 'UTF-8')
            if encoding:
                header[k.ContentEncoding] = encoding.encode(encoding=encoding or 'UTF-8')
        return Header(header)

    def update(self, another):
        if isinstance(another, Header):
            another = another.header
        self.header.update(another)

    def to_bytes(self):
        bio = io.BytesIO()
        for key, value in self.header.items():
            bio.write(key.to_bytes())
            bio.write(b': ')
            bio.write(value)
            bio.write(NEWLINE_BYTES)
        return bio.getvalue()


class RequestError(TeaportError):
    def __init__(self, what: str, raw_exception: Exception=None, value: bytes=None):
        super().__init__(self)
        self.what = what
        self.raw_exception = raw_exception
        self.value = value

    def to_response(self):
        return Response(code=ResponseStatusCode(400))


class Request(FromBytes):
    def __init__(self, method: RequestMethod, url: str, version: HTTPVersion, header: Header, data: bytes=None):
        self.method, self.url, self.version, self.header, self.data = method, url, version, header, data

    @classmethod
    def from_bytes(cls, value: io.BytesIO) -> 'Request':
        try:
            request_line = value.readline().strip()
            method, url, version = request_line.split(b' ', maxsplit=2)
            method = RequestMethod.from_bytes(method)
            url = url.decode()
            version = HTTPVersion.from_bytes(version)
            headers = []
            for header in value:
                header = header.strip()
                if len(header) == 0:
                    break
                else:
                    headers.append(header)
            header = Header.from_header(headers)
            data = value.read()
        except Exception as e:
            tb = sys.exc_info()[2]
            raise RequestError('Cannot understand raw request', e).with_traceback(tb)
        else:
            return Request(method, url, version, header, data or None)


class ResponseError(TeaportError):
    def __init__(self, code: int, raw_exception: Exception = None, request: Request = None):
        super().__init__(self)
        self.code = ResponseStatusCode(code)
        self.raw_exception = raw_exception
        self.request = request

    def to_response(self) -> 'Response':
        return Response(code=self.code)


def guess_encoding(data: bytes, threshold: float = 0.95, max_length: int = 128) -> typing.Optional[str]:
    det = chardet.detect(data[max_length:])
    if det['confidence'] >= threshold:
        return det['encoding']


class Response(ToBytes):
    def __init__(self, code: ResponseStatusCode,
                 header: Header = None,
                 content: bytes = None,
                 version: HTTPVersion = HTTPVersion.HTTP_1_1,
                 encoding: str = 'UTF-8'):
        self.version = version
        self.code = code
        self.content = content
        self.header = header
        self.encoding = encoding

    @staticmethod
    def html(content: str, encoding='UTF-8') -> 'Response':
        content = content.encode(encoding=encoding)
        header = Header.from_content('text/html', len(content), encoding)
        return Response(code=ResponseStatusCode(200), header=header, content=content)

    @staticmethod
    def static_file(path: str) -> 'Response':
        # TODO: Cache this
        with open(path, 'rb') as fd:
            content = fd.read()
        mime, encoding = mimetypes.guess_type(path)
        # 成功推测 MIME 时, 进一步推测编码
        if mime is not None:
            if encoding is None and mime.startswith('text/'):
                encoding = guess_encoding(content)
        else:  # 无法推测 MIME 时，更可能是二进制数据
            mime = f'application/{os.path.splitext(path)[-1][1:]}'
        header = Header.from_content(mime, len(content), encoding)
        return Response(code=ResponseStatusCode(200), header=header, content=content)

    @staticmethod
    def code(code: ResponseStatusCode) -> 'Response':
        return Response(code=code)

    def to_bytes(self) -> bytes:
        try:
            bio = io.BytesIO()
            bio.write(self.version.to_bytes())
            bio.write(b' ')
            bio.write(self.code.to_bytes())
            bio.write(NEWLINE_BYTES)
            if self.header:
                bio.write(self.header.to_bytes())
                bio.write(NEWLINE_BYTES)
            if self.content:
                bio.write(self.content)
                bio.write(NEWLINE_BYTES)
        except Exception as e:
            tb = sys.exc_info()[2]
            raise ResponseError(500, raw_exception=e).with_traceback(tb)
        else:
            return bio.getvalue()


class Teaport:
    def __init__(self):
        self.router = collections.defaultdict(dict)
        self.logger = logging.getLogger('Teaport')

    def register(self, methods: [RequestMethod], pattern: str, fn):
        for m in methods:
            self.router[m][re.compile(pattern)] = fn

    def get(self, pattern: str):
        return functools.partial(self.register, [RequestMethod.GET], pattern)

    def post(self, pattern: str):
        return functools.partial(self.register, [RequestMethod.POST], pattern)

    def route(self, request: Request) -> Response:
        table = self.router[request.method]
        for pattern, fn in table.items():
            m = pattern.match(request.url)
            if m is not None:
                try:
                    res = fn(request, *m.groups())
                    if isinstance(res, ResponseStatusCode):
                        res = Response.code(res)
                    return res
                except TeaportError as te:
                    return te.to_response()
                except Exception as e:
                    raise ResponseError(500, raw_exception=e)
        else:
            raise ResponseError(404, request=request)

    def serve(self, host: str, port: int, debuging: bool = False):
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            s.bind((host, port))
            s.listen(1)
            while True:
                connection, client_address = s.accept()
                with connection:
                    try:
                        raw_request = SocketIO.read_from(connection)
                        request = Request.from_bytes(raw_request)
                        response = self.route(request)
                    except IOError as error:
                        response = Response.code(ResponseStatusCode(500))
                        self.logger.exception(error)
                    except TeaportError as error:
                        response = error.to_response()
                        self.logger.exception(error)
                    finally:
                        SocketIO.write_to(connection, response.to_bytes())


app = Teaport()


@app.get(r'/add/(\d+)/(\d+)')
def static_html(request: Request, lhs: str, rhs: str) -> 'Response':
    return Response.html(f'<html>\n\t<h1>{lhs} + {rhs} = {int(lhs) + int(rhs)}</h1>\n<html>')


@app.post(r'add/(\d+)')
def add_with_data(request: Request, lhs: str) -> Response:
    lhs = int(lhs)
    try:
        rhs = int(request.data)
    except ValueError:
        rhs = 0
    return Response.html(f'<html>\n\t<h1>{lhs} + {rhs} = {int(lhs) + int(rhs)}</h1>\n<html>')


@app.get(r'/src/(.*)')
def src(request: Request, path: str) -> 'Response':
    if os.path.exists(path):
        return Response.static_file(path)
    else:
        raise ResponseError(404, request=request)


if __name__ == '__main__':
    app.serve(host='', port=80)
