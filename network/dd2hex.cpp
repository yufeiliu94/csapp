/**
 * IPv4 十六进制 <- 点分十进制
 */

#include <iostream>
#include "csapp.hpp"
#include "csapp/maybe.hpp"

using csapp::maybe;
using csapp::nothing;

maybe<uint32_t> parse(const std::string &dd) {
  uint32_t address = 0, part = 0;
  size_t dots = 3, bytes = 4;
  for (auto digit : dd) {
    if (digit == '.') {
      if (bytes == dots and dots > 0) {
        address |= part << (8 * dots);
        part = 0;
        dots -= 1;
      } else {
        break;
      }
    } else if (std::isdigit(digit)) {
      bytes = std::min(dots, bytes);
      part = part * 10 + digit - '0';
    } else {
      break;
    }
  }
  address |= part;
  if (dots == 0 and bytes == 0) {
    return address;
  } else {
    return nothing;
  }
}

std::string format(uint32_t address) {
  char buffer[11];
  sprintf(buffer, "0x%x", address);
  return std::string(buffer);
}

constexpr auto usage = "Usage: dd2hex [address]";

int main(int argc, char *argv[]) {
  const auto dd = std::string(argv[argc - 1]);
  auto hex = parse(dd).fmap(format);
  if (hex.has_value()) {
    std::cout << *hex << std::endl;
  } else {
    std::cout << usage << std::endl;
  }
  return 0;
}
