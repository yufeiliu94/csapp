/**
 * The Tiny Web Server
 */

#include <netinet/in.h>
#include <unistd.h>
#include <cstring>
#include <iostream>
#include <sstream>
#include "csapp.hpp"
#include "csapp/rio.hpp"
#include "csapp/socket.hpp"

enum Method { GET };
enum Version { UNKNOWN, HTTP_1_0, HTTP_1_1, HTTP_2_0 };
using URL = std::string;
using RequestLine = std::tuple<Method, URL, Version>;

/**
 * GET / HTTP/1.1<CR>
 * @param rl
 * @param len
 * @return
 */
RequestLine parse_request_line(const char *rl, const size_t len) {
  const auto version_cursor = rl + len - 8;
  Version version = UNKNOWN;
  if (strncmp(rl, "GET ", 4) != 0) {
    throw 501;
  }
  if (strncmp(version_cursor - 1, " HTTP/", 6) != 0) {
    throw 505;
  }
  if (strncmp(version_cursor + 5, "1.0", 3) == 0) {
    version = HTTP_1_0;
  } else if (strncmp(version_cursor + 5, "1.1", 3) == 0) {
    version = HTTP_1_1;
  } else if (strncmp(version_cursor + 5, "2.0", 3) == 0) {
    version = HTTP_2_0;
  } else {
    throw 505;
  }
  const auto url_begin = rl + 4, url_end = version_cursor - 1;
  return std::make_tuple(GET, std::string(url_begin, url_end), version);
}

static const size_t buffer_size = 1024;

void doit(int request_fd) {
  csapp::rio::rio_t rio(request_fd);
  char buffer[buffer_size];
  // REQUEST LINE
  const auto request_line_length = rio.readline(buffer, buffer_size);
  const auto request_line = parse_request_line(buffer, request_line_length);
}

uint16_t parse_port(const char *port) {
  uint16_t value;
  std::istringstream(port) >> value;
  return value;
}

void main_loop(int argc, char *argv[]) {
  if (argc != 2) {
    std::cerr << "Usage: tiny [PORT]" << std::endl;
    std::terminate();
  }
  // TODO catch bad parameter
  const uint16_t port = parse_port(argv[1]);
  const auto fd = csapp::socket::open_listenfd(port);
  while (true) {
    sockaddr_in client{};
    socklen_t length = sizeof(client);
    const auto request =
        accept(fd, reinterpret_cast<sockaddr *>(&client), &length);
    doit(request);
    close(request);
    break;
  }
}

#define CATCH_CONFIG_MAIN
#include <catch.hpp>

TEST_CASE("parse_request_line", "[TINY]") {
  const auto rl = parse_request_line("GET /index.html HTTP/1.1", 24);
  REQUIRE(std::get<0>(rl) == GET);
  REQUIRE(std::get<1>(rl) == "/index.html");
  REQUIRE(std::get<2>(rl) == HTTP_1_1);
  bool failure = false;
  try {
    failure = false;
    parse_request_line("POST /index.html HTTP/1.1", 25);
  } catch (const int &code) {
    REQUIRE(code == 501);
    failure = true;
  }
  REQUIRE(failure);
  try {
    failure = false;
    parse_request_line("GET /index.html HTTP/1.11", 25);
  } catch (const int &code) {
    REQUIRE(code == 505);
    failure = true;
  }
  REQUIRE(failure);
}
