/**
 * IPv4 十六进制 -> 点分十进制
 */

#include <iostream>
#include "csapp.hpp"
#include "csapp/maybe.hpp"

using csapp::maybe;
using csapp::nothing;

int parse_digit(int digit) {
  int value = 0;
  if (std::isdigit(digit)) {
    value = digit - '0';
  } else if ('A' <= digit and digit <= 'F') {
    value = digit - 'A' + 10;
  } else if ('a' <= digit and digit <= 'f') {
    value = digit - 'a' + 10;
  } else {
    return -1;
  }
  return value;
}

maybe<uint32_t> parse(const std::string &hex) {
  uint32_t address = 0;
  int part;
  if (hex[0] != '0' or hex[1] != 'x' or hex.length() != 10) {
    return nothing;
  }
  for (auto i = 0; i < 8; ++i) {
    part = parse_digit(hex[2 + i]);
    if (part >= 0) {
      address |= part << (4 * (7 - i));
    } else {
      return nothing;
    }
  }
  return address;
}

std::string format(uint32_t address) {
  char buffer[16];
  const auto a = (address & 0xFF000000) >> 24;
  const auto b = (address & 0x00FF0000) >> 16;
  const auto c = (address & 0x0000FF00) >> 8;
  const auto d = (address & 0x000000FF);
  sprintf(buffer, "%d.%d.%d.%d", a, b, c, d);
  return std::string(buffer);
}

constexpr auto usage = "Usage: hex2dd [address]";

int main(int argc, char *argv[]) {
  const auto hex = std::string(argv[argc - 1]);
  auto dd = parse(hex).fmap(format);
  if (dd.has_value()) {
    std::cout << *dd << std::endl;
  } else {
    std::cout << usage << std::endl;
  }
  return 0;
}
