# encoding: UTF-8

import io
import os
import abc
import itertools
from csapp.type import Show
from typing import Text, Iterable, Any, Optional, TextIO, Union
from collections import namedtuple
import enum


LanguageProperty = namedtuple('LanguageProperty', ['code_name', 'extension', 'inline_comment_pattern'])


class Language(enum.Enum):
    C = LanguageProperty('c', 'c', '// {}')
    CPP = LanguageProperty('cpp', 'cpp', '// {}')
    ES6 = LanguageProperty('javascript', 'js', '// {}')
    Python = LanguageProperty('python3', 'py', '# {}')
    Haskell = LanguageProperty('haskell', 'hs', '-- {}')

    @classmethod
    def guess(cls, filename: str) -> Optional['Language']:
        if not isinstance(filename, str):
            return None
        _, ext = os.path.splitext(filename)
        if not ext.startswith('.'):
            return None
        ext = ext[1:]
        for language in iter(cls):
            if language.ext == ext:
                return language
        else:
            return None

    @property
    def code_name(self) -> str:
        return self.value.code_name

    @property
    def ext(self) -> str:
        return self.value.extension

    def inline_comment(self, comment: str) -> str:
        return self.value.inline_comment_pattern.format(comment)


class Component(Show, abc.ABC):
    pass

class Code(Component):
    def __init__(self, code: str, name: str = None, language: Language = None):
        self.code, self.name, self.language = code, name, language or Language.guess(name)
        self.code = os.linesep.join(self.code.strip().splitlines())

    def show(self, stream: Optional[TextIO] = None, indent: int = 0):
        ss = stream or io.StringIO()
        if self.language is not None:
            ss.write(f'```{self.language.code_name}{os.linesep}')
            if self.name is not None:
                ss.write(f'{self.language.inline_comment(self.name)}{os.linesep}')
        else:
            ss.write(f'```{os.linesep}')
        ss.write(self.code)
        ss.write(f'{os.linesep}```{os.linesep}')
        if stream is None:
            return ss.getvalue()

class Table(Component):
    def __init__(self, columns: [Text], zh_width: int = 2, name: str = None):
        self.zh_width = zh_width
        self.rows = [columns]
        self.name = name

    def __zh_len__(self, s: Text) -> int:
        return sum(ord(c) < 256 or self.zh_width for c in s)

    def __compile__(self) -> [Text]:
        # 以标题行初始化每列的最大宽度
        column_widths = [self.__zh_len__(t) for t in self.title]
        max_columns = len(column_widths)
        # 格式化表格中每一项
        rows = []
        for i in range(1, len(self.rows)):
            # 以表头为参考统一每一行的列数
            row = [Show.to_string(term) for term in self.rows[i][:max_columns]]
            if len(row) < max_columns:
                row.extend(itertools.repeat('', times=max_columns-len(row)))
            rows.append(row)
            # 并更新每一列的最大宽度
            column_widths = [max(l, r) for l, r in zip(column_widths, map(self.__zh_len__, row))]
        title, sep = io.StringIO(), io.StringIO()
        # 生成表头
        for column, size in zip(self.title, column_widths):
            title_padding = ' ' * max(0, size - self.__zh_len__(column))
            sep_padding = '-' * size
            title.write(f'| {column}{title_padding} ')
            sep.write(f'| {sep_padding} ')
        for ss in (title, sep):
            ss.write('|')
            ss.write(os.linesep)
        compiled = [title.getvalue(), sep.getvalue()]
        # 生成数据行
        for row in rows:
            pattern = io.StringIO()
            for j, ref in enumerate(column_widths):
                pattern.write(f'| {{:{ref + len(row[j]) - self.__zh_len__(row[j])}s}} ')
            pattern.write('|')
            pattern.write(os.linesep)
            compiled.append(pattern.getvalue().format(*row))
        return compiled

    @property
    def title(self) -> [Text]:
        return self.rows[0]

    @title.setter
    def title(self, title: [Text]):
        self.rows[0] = title

    def expend(self, row: Iterable[Any]) -> 'Table':
        self.rows.append(row)
        return self

    def append(self, *row: Any) -> 'Table':
        self.rows.append(row)
        return self

    def show(self, stream: Optional[TextIO] = None, indent: int = 0):
        pad = ' ' * indent
        ss = stream or io.StringIO()
        for line in self.__compile__():
            ss.write(pad)
            ss.write(line)
        if stream is None:
            return ss.getvalue()


class Document(Show):
    def __init__(self):
        self.components = []

    def __lshift__(self, other: Union[Text, Component]) -> 'Document':
        self.components.append(other)
        return self

    def show(self, stream: Optional[TextIO] = None, indent: int = 0):
        ss = stream or io.StringIO()
        for c in self.components:
            if isinstance(c, Text):
                ss.write(c)
                if not c.endswith(os.linesep):
                    ss.write(os.linesep)
            elif isinstance(c, Component):
                c.show(ss, indent)
            else:
                ss.write(str(c))
                ss.write(os.linesep)
        if stream is None:
            return ss.getvalue()
