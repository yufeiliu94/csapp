#include "csapp/rio.hpp"
#include <unistd.h>
#include <algorithm>
#include <cstring>
#include "csapp/debug.hpp"

namespace csapp {
namespace rio {

/**
 * 无缓冲的 I/O
 */

ssize_t readn(int fd, void* dest, size_t n) {
  auto left = n;
  char* dest_cursor = reinterpret_cast<char*>(dest);
  while (left > 0) {
    auto n_read = ::read(fd, dest_cursor, left);
    if (n_read < 0) {
      if (errno == EINTR) {
        return 0;
      } else {
        return -1;
      }
    } else if (n_read == 0) {
      break;
    }
    left -= n_read;
    dest_cursor += n_read;
  }
  return n - left;
}

ssize_t writen(int fd, const void* source, size_t n) {
  size_t left = n;
  auto source_cursor = reinterpret_cast<const char*>(source);
  while (left > 0) {
    auto n_written = ::write(fd, source_cursor, left);
    if (n_written <= 0) {
      if (errno == EINTR) {
        n_written = 0;
      } else {
        return -1;
      }
    }
    left -= n_written;
    source_cursor += n_written;
  }
  return n;
}

/**
 * 带缓冲的输入
 */

rio_t::rio_t(int fd, size_t buffer_size)
    : fd(fd),
      counter(0),
      cursor(nullptr),
      buffer_size(buffer_size),
      buffer(std::make_unique<char[]>(buffer_size)) {}

ssize_t rio_t::read(char* dest, size_t n) {
  while (counter <= 0) {
    counter = ::read(fd, buffer.get(), buffer_size);
    if (counter < 0) {
      if (errno != EINTR) { /* I/O 被中断 */
        return -1;
      }
    } else if (counter == 0) { /* EOF */
      return 0;
    } else {
      cursor = buffer.get();
    }
  }
  auto actual_read = std::min(n, static_cast<size_t>(counter));
  std::memcpy(dest, cursor, actual_read);
  cursor += actual_read;
  counter -= actual_read;
  return actual_read;
}

ssize_t rio_t::readn(void* dest, size_t n) {
  auto left = n;
  char* dest_cursor = reinterpret_cast<char*>(dest);
  while (left > 0) {
    auto n_read = read(dest_cursor, left);
    if (n_read < 0) {
      if (errno == EINTR) {
        return 0;
      } else {
        return -1;
      }
    } else if (n_read == 0) {
      break;
    }
    left -= n_read;
    dest_cursor += n_read;
  }
  return n - left;
}

ssize_t rio_t::readline(void* dest, size_t max_len) {
  size_t n = 1;
  char current, *dest_cursor = reinterpret_cast<char*>(dest);
  for (; n < max_len; ++n) {
    const auto rc = read(&current, 1);
    if (rc == 1) {
      *dest_cursor++ = current;
      if (current == '\n') {
        break;
      } else {
      }
    } else if (rc == 0) {
      if (n == 1) {
        return 0;
      } else {
        break;
      }
    } else {
      return -1;
    }
  }
  *dest_cursor = 0;
  return n;
}

}  // namespace rio
}  // namespace csapp
