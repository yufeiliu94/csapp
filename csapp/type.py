# encoding: UTF-8

"""
用于假装自己写的是 Haskell  的模块
"""

import abc
import io
from typing import Optional, TextIO, Text


class Show:
    @abc.abstractmethod
    def show(self, stream: Optional[TextIO] = None, indent: int = 0) -> Optional[Text]:
        """
        格式化对象到人类可读格式
        :param stream: 可选的输出流。此参数非 None 时，结果被写入 stream，返回值为 None
        :param indent: 可选的缩进值
        :return: 不给定 stream 时，返回结果；否则，返回 None
        """
        pass

    @staticmethod
    def to_string(obj) -> Text:
        if isinstance(obj, Show):
            return obj.show()
        return str(obj)


class Eq:
    @abc.abstractmethod
    def __eq__(self, other: 'Eq') -> bool:
        pass

    def __ne__(self, other: 'Eq') -> bool:
        return not self == other


class Ord(Eq):
    @abc.abstractmethod
    def __lt__(self, other: 'Ord') -> bool:
        pass
