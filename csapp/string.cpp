#include "csapp/string.hpp"

namespace csapp {
namespace string {

std::vector<std::string> from_array(const char **strings) {
  std::vector<std::string> xs;
  for (auto i = 0; strings != nullptr and strings[i] != nullptr; ++i) {
    xs.emplace_back(strings[i]);
  }
  return xs;
}

}  // namespace string
}  // namespace csapp
