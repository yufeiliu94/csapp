#include "csapp.hpp"
#include <cerrno>
#include <cstring>
#include <string>
#include <system_error>
#include "csapp/error.hpp"

namespace csapp {
/**
 * CSAPP 错误处理工具包
 */

UnixError::UnixError(const std::string &hint)
    : std::system_error(std::error_code(errno, std::system_category()),
                        hint + std::strerror(errno)) {}

UnixErrorHandler::UnixErrorHandler(const std::string hint) : hint{hint} {}

}  // namespace csapp
