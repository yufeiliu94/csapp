#include "csapp/socket.hpp"
#include <netdb.h>
#include <algorithm>
#include <cstring>
#include "csapp.hpp"
#include "csapp/debug.hpp"
#include "csapp/error.hpp"

namespace csapp {
namespace socket {

template <typename T>
inline void clear_bytes(T &target) {
  bzero(reinterpret_cast<void *>(&target), sizeof(T));
}

template <typename T, typename Integral>
inline void copy_bytes(T &target, const char *source, Integral size) {
  const auto copy_size = std::min(Integral{sizeof(T)}, std::max(size, 0));
  DEBUG_MESSAGE("[copy_bytes] copy_size = ", copy_size);
  bcopy(source, reinterpret_cast<char *>(&target), copy_size);
}

/** 请求队列的最大长度 (listen 函数的 backlog 参数) */
constexpr int LISTEN_MAX_QUEEN_LENGTH = 1 << 16;

/**
 * 打开并返回一个客户端套接字
 */
int open_clientfd(const char *hostname, uint16_t port) {
  int fd;
  hostent *host;
  sockaddr_in svr{};
  UnixErrorHandler e("[OpenClientFD] ");

  // 创建文件描述符
  e.tie(fd) << ::socket(AF_INET, SOCK_STREAM, 0);

  // 设置地址
  DEBUG_MESSAGE("[OpenClientFD] hostname = ", hostname);
  e.tie(host) << gethostbyname(hostname);
  clear_bytes(svr);
  DEBUG_MESSAGE("[OpenClientFD] h_addr_list[0] = ", host->h_addr_list[0]);
  DEBUG_MESSAGE("[OpenClientFD] h_length = ", host->h_length);
  copy_bytes(svr.sin_addr.s_addr, host->h_addr_list[0], host->h_length);
  svr.sin_family = AF_INET;
  svr.sin_port = htons(port);

  // 连接
  e << connect(fd, (sockaddr *)&svr, sizeof(svr));
  return fd;
}

/**
 * 打开并返回一个监听套接字
 */
int open_listenfd(uint16_t port) {
  int fd;
  int optval;
  auto optval_ptr = static_cast<void *>(&optval);
  UnixErrorHandler e("[OpenListenFD] ");

  // 设置地址
  sockaddr_in server_address{};
  clear_bytes(server_address);
  server_address.sin_family = AF_INET;
  server_address.sin_port = htons(port);
  server_address.sin_addr.s_addr = htonl(INADDR_ANY);

  // 操纵 Socket
  e.tie(fd) << ::socket(AF_INET, SOCK_STREAM, 0)
            << setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, optval_ptr,
                          sizeof(optval))
            << bind(fd, (sockaddr *)(&server_address), sizeof(server_address))
            << listen(fd, LISTEN_MAX_QUEEN_LENGTH);
  return fd;
}

}  // namespace socket
}  // namespace csapp
