#include <functional>

namespace csapp {
namespace functional {

template <typename Signature>
struct function_t;

template <typename Return, typename... Arguments>
struct function_t<Return(Arguments...)> {
  constexpr size_t args = sizeof...(Arguments);
  using return_type = Return;
  using type = Return (*)(Arguments...);
};

}  // namespace functional
}  // namespace csapp
