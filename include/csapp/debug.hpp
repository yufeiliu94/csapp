#pragma once
#include <type_traits>

#if (!defined(NDEBUG))
#define DEBUG_PRINT(what) csapp::debug::message(#what, what, std::clog)
#define DEBUG_PRINT_TO(what, os) csapp::debug::message(#what, what, os)
#define DEBUG_MESSAGE(msg, what) csapp::debug::message(msg, what, std::clog)
#define DEBUG_MESSAGE_TO(msg, what, os) csapp::debug::message(msg, what, os)
#include <iostream>
#include <string>
namespace csapp {
namespace debug {
template <typename Printable>
void message(const std::string what, const Printable& printable,
             std::ostream& os) {
  os << what << printable << std::endl;
}
}  // namespace debug
}  // namespace csapp
#else
#define DEBUG_PRINT(what)
#define DEBUG_PRINT_TO(what, os)
#define DEBUG_MESSAGE(msg, what)
#define DEBUG_MESSAGE_TO(msg, what, os)
#endif

namespace csapp {
namespace debug {

/**
 * 利用 GCC 7.2 错误信息的编译期 Debug 支持
 * 通过产生 std::enable_if<false> 般的 ill-formed 表达式
 * 令编译器输出适当的错误信息
 */
template <typename T, typename U>
struct is_same {};

template <typename T>
struct is_same<T, T> {
  static constexpr bool value = true;
};

template <typename LHS, typename RHS>
constexpr bool assert_same_type = is_same<LHS, RHS>::value;

}  // namespace debug
}  // namespace csapp
