#pragma once

#include <memory>
#include "csapp.hpp"

/**
 * CSAPP 中的 Robust I/O
 */

namespace csapp {
namespace rio {

/**
 * 无缓冲的 I/O
 */

ssize_t readn(int fd, void *dest, size_t n);
ssize_t writen(int fd, const void *data, size_t n);

/**
 * 带缓冲的输入
 */

class rio_t {
 public:
  rio_t() = delete;
  rio_t(int fd, size_t buffer_size = 8192);
  ssize_t readn(void *dest, size_t n);
  ssize_t writen(const void *source, size_t n);
  ssize_t readline(void *dest, size_t n);

 private:
  int fd;                          // 文件描述符
  int counter;                     // 缓冲区内剩余字符数
  char *cursor;                    // 缓冲区中的下一个未读位置
  const size_t buffer_size;        // 缓冲区大小
  std::unique_ptr<char[]> buffer;  // 缓冲区
  ssize_t read(char *dest, size_t n);
};

std::shared_ptr<rio_t> open(const std::string path);

}  // RIO
}  // namespace CSAPP
