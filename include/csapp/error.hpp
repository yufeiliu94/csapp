/**
 * CSAPP 错误处理工具包
 */

#pragma once

#include <string>
#include <system_error>
#include <type_traits>

namespace csapp {

struct UnixError : std::system_error {
  UnixError(const std::string &hint);
};

template<typename T>
struct UnixErrorHandlerSlot;

template<typename T>
constexpr bool should_compare_with_null
    = std::is_pointer<T>::value or std::is_null_pointer<T>::value;

template<typename T>
constexpr bool should_compare_with_integral = std::is_integral<T>::value;

/**
 * 使用流式语法的 Unix 风格错误处理器
 */
struct UnixErrorHandler {
  UnixErrorHandler(const std::string hint);

  template<bool Cond>
  using bool_if = typename std::enable_if<Cond, bool>::type;

  /** 针对指针类型的 is_error */
  template<typename Pointer>
  bool_if<should_compare_with_null<Pointer>> is_error(Pointer pointer) {
    return pointer == nullptr;
  }

  /** 针对有符号整数类型的 is_error */
  template<typename Integral>
  bool_if<should_compare_with_integral<Integral>> is_error(Integral value) {
    return value < 0;
  };

  /** 处理 Unix 风格的返回值 */
  template<typename ReturnType>
  void handle(ReturnType rtv) {
    if (is_error(rtv)) {
      throw UnixError(hint);
    }
  }

  /** 绑定对象 slot 以实现在赋值之前处理错误 */
  template<typename ReturnType>
  UnixErrorHandlerSlot<ReturnType> tie(ReturnType &slot) {
    return UnixErrorHandlerSlot<ReturnType>(slot, *this);
  }

  /** handler = error */
  template<typename ReturnType>
  UnixErrorHandler &operator=(ReturnType rtv) {
    handle(rtv);
    return *this;
  }

  /** handler << error */
  template<typename ReturnType>
  UnixErrorHandler &operator<<(ReturnType rtv) {
    handle(rtv);
    return *this;
  }
  const std::string hint;
};

/**
 * UnixErrorHandlerSlot 用于赋值的槽
 */
template<typename T>
struct UnixErrorHandlerSlot {
  UnixErrorHandlerSlot(T &slot, UnixErrorHandler &handler)
      : slot{slot}, handler{handler} {}

  /** slot << value 流式 API 返回对错误处理器 Handler 的引用 */
  UnixErrorHandler &operator<<(T rtv) {
    handler.handle(rtv);
    slot = rtv;
    return handler;
  }

  /** slot = value 声明式 API 返回对被代理对象 slot 的引用 */
  T &operator=(T rtv) {
    handler.handle(rtv);
    return slot = rtv;
  }

  T &slot;
  UnixErrorHandler &handler;
};

}  // namespace csapp
