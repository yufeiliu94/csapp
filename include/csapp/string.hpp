#include <cstring>
#include <string>
#include <type_traits>
#include <vector>
#include "csapp.hpp"
#include "csapp/maybe.hpp"

namespace csapp {
namespace string {

template <typename T>
using stdlib_strtoi = T (*)(const char *, char **, int);

template <typename T>
maybe<T> stdlib_cast(stdlib_strtoi<T> fn, const char *literal, int base = 10) {
  const auto end = literal + std::strlen(literal);
  char *stop;
  const auto value = fn(literal, &stop, base);
  if (stop == end and errno == 0) {
    return value;
  } else {
    return nothing;
  }
}

template <typename T>
using stdlib_strtof = T (*)(const char *, char **);
template <typename T>
maybe<T> stdlib_cast(stdlib_strtof<T> fn, const char *literal) {
  const auto end = literal + std::strlen(literal);
  char *stop;
  const auto value = fn(literal, &stop);
  if (stop == end and errno == 0) {
    return value;
  } else {
    return nothing;
  }
}

std::vector<std::string> from_array(const char **);

}  // namespace string
}  // namespace csapp
