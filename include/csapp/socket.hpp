#pragma once
#include <arpa/inet.h>
#include <sys/socket.h>
#include "csapp.hpp"

namespace csapp {
namespace socket {
/**
 * 打开并返回一个客户端套接字
 */
int open_clientfd(const char *hostname, uint16_t port);
/**
 * 打开并返回一个监听套接字
 */
int open_listenfd(uint16_t port);

}  // namespace socket
}  // namespace csapp
