/**
 * 一个凑合能用的 optional 实现
 */

#pragma once

#include <memory>
#include <optional>
#include <type_traits>

namespace csapp {

using nothing_t = std::nullopt_t;
inline constexpr auto nothing = std::nullopt;

template <typename T>
class maybe {
 public:
  constexpr maybe(const nothing_t) : opt{std::nullopt} {}
  constexpr maybe(const T &data) : opt{data} {};
  constexpr maybe(const maybe<T> &another) : opt{another.opt} {}

  constexpr const T *operator->() const { return opt.operator->(); }
  constexpr const T &operator*() const & { return *opt; }
  constexpr const T &&operator*() const && { return *opt; }
  constexpr T *operator->() { return opt.operator->(); }
  constexpr T &operator*() & { return *opt; }
  constexpr T &&operator*() && { return *opt; }

  constexpr bool has_value() const { return opt.has_value(); }
  constexpr bool is_nothing() const { return not has_value(); }

  template <typename ReturnType>
  inline maybe<ReturnType> fmap(ReturnType (*callable)(T)) {
    if (this->is_nothing()) {
      return nothing;
    } else {
      return callable(*opt);
    }
  };

 private:
  std::optional<T> opt;
};

}  // namespace csapp
