#define CATCH_CONFIG_MAIN
#include <catch.hpp>
#include "csapp/error.hpp"
#include "csapp/maybe.hpp"

using namespace csapp;

template <typename T>
constexpr bool same_size_for = sizeof(csapp::maybe<T>) ==
                               sizeof(std::optional<T>);

TEST_CASE("maybe", "[CSAPP]") {
  // 大小应等于 std::optional
  REQUIRE(same_size_for<int>);
  REQUIRE(same_size_for<double>);
  REQUIRE(same_size_for<std::string>);
  REQUIRE(same_size_for<std::map<std::string, std::vector<bool>>>);
}

TEST_CASE("UnixErrorHandler", "[CSAPP]") {
  UnixErrorHandler e("[CSAPP::UnitTest] ");

  // 能够正确处理赋值
  int value = 0;
  int *pointer = nullptr;
  e.tie(value) << 42;
  e.tie(pointer) << &value;
  REQUIRE(value == 42);
  REQUIRE(pointer == &value);

  // 遇到负值或空指针时抛出异常
  REQUIRE_THROWS_AS(e.handle(-1), UnixError);
  REQUIRE_THROWS_AS(e.handle(nullptr), UnixError);
}
