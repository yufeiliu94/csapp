#define CATCH_CONFIG_MAIN
#include "csapp/socket.hpp"
#include <netinet/in.h>
#include <unistd.h>
#include <catch.hpp>

const auto hostname = "127.0.0.1";
const auto port = 20000;

TEST_CASE("they works", "[CSAPP::Socket]") {
  const auto client_pid = fork();
  if (client_pid == 0) {
    sleep(1);
    auto fd = csapp::socket::open_clientfd(hostname, port);
    write(fd, "HELLO", 6);
    close(fd);
    exit(0);
  } else {
    socklen_t length;
    sockaddr_in client{};
    const auto server_fd = csapp::socket::open_listenfd(port);
    auto fd = accept(server_fd, (sockaddr *)(&client), &length);
    char buffer[6];
    read(fd, buffer, 6);
    close(fd);
    REQUIRE(std::string(buffer) == "HELLO");
  }
}
