#define CATCH_CONFIG_RUNNER
#include "csapp/rio.hpp"
#include <fcntl.h>
#include <unistd.h>
#include <catch.hpp>
#include <cstdio>
#include <fstream>
#include "csapp.hpp"

const char *foobar = "foobar";
const char *foobar_txt = "foobar.txt";

void setup() {
  // 创建 foobar.txt
  auto os = std::ofstream(foobar_txt);
  for (auto i = 0; i < 16; ++i) {
    os << foobar << " " << i << std::endl;
  }
  os.close();
}

void cleanup() { std::remove(foobar_txt); }

TEST_CASE("read", "[Robust I/O]") {
  using csapp::rio::readn;
  auto fd = ::open(foobar_txt, O_RDONLY);
  char buffer[16] = {};
  const auto three = readn(fd, buffer, 3);
  REQUIRE(three == 3);
  REQUIRE(std::string(buffer) == "foo");
  close(fd);
}

TEST_CASE("rio_t", "[Robust I/O]") {
  using csapp::rio::rio_t;
  auto fd = ::open(foobar_txt, O_RDONLY);
  rio_t rio(fd, 4);
  char buffer[16] = {};
  const auto three = rio.readn(buffer, 3);
  REQUIRE(three == 3);
  REQUIRE(std::string(buffer) == "foo");
  const auto six = rio.readline(buffer + 3, 13);
  REQUIRE(six == 6);
  REQUIRE(std::string(buffer) == "foobar 0\n");
  const auto nine = rio.readline(buffer, 16);
  REQUIRE(nine == 9);
  REQUIRE(std::string(buffer) == "foobar 1\n");
  close(fd);
}

TEST_CASE("write", "[Robust I/O]") {
  using csapp::rio::rio_t;
  using csapp::rio::writen;
  auto fout = ::open(foobar_txt, O_WRONLY);
  auto seven = csapp::rio::writen(fout, "foobar\n", 7);
  close(fout);
  REQUIRE(seven == 7);
  auto fin = ::open(foobar_txt, O_RDONLY);
  char buffer[16] = {};
  rio_t rin(fin);
  seven = rin.readline(buffer, 16);
  REQUIRE(seven == 7);
  REQUIRE(std::string(buffer) == "foobar\n");
}

int main(int argc, char *argv[]) {
  setup();
  auto rtv = Catch::Session().run(argc, argv);
  cleanup();
  return rtv;
}
