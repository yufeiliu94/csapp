#define CATCH_CONFIG_MAIN
#include "csapp/string.hpp"
#include <catch.hpp>
#include "csapp/debug.hpp"

#define TEST(fn) TEST_CASE(#fn, "[csapp::string]")
using namespace csapp::string;

TEST(stdlib_cast) {
  auto i42 = stdlib_cast(std::strtol, "42");
  auto fpi = stdlib_cast(std::strtof, "3.14159");
  auto i66 = stdlib_cast(std::strtoll, "42", 16);
  using csapp::debug::assert_same_type;
  const auto type_matches =
      assert_same_type<csapp::maybe<long int>, decltype(i42)> and
      assert_same_type<csapp::maybe<float>, decltype(fpi)> and
      assert_same_type<csapp::maybe<long long int>, decltype(i66)>;
  REQUIRE(*i42 == 42);
  REQUIRE(*i66 == 66);
  REQUIRE(*fpi == 3.14159f);
  REQUIRE(type_matches);
}

TEST(from_array) {
  const char *strings[] = {"Hello", "world", nullptr};
  const auto array = from_array(strings);
  REQUIRE(array.size() == 2);
  REQUIRE(array[0] == strings[0]);
  REQUIRE(array[1] == strings[1]);
  const auto empty = from_array(nullptr);
  REQUIRE(empty.size() == 0);
}
