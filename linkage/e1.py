# encoding: UTF-8

from csapp.markdown import Table, Code, Document

swap_c = Code(name='swap.c', code="""
extern int buf[];

int *bufp0 = &buf[0];
int *bufp1;

void swap() {
  int temp;
  bufp1 = &buf[1];
  temp = *bufp0;
  *bufp0 = *bufp1;
  *bufp0 = temp;
}""")

swap_o = Code(name='swap.o', code="""
0000000000000000         *UND*	0000000000000000 buf
0000000000000000 g     O .data	0000000000000008 bufp0
0000000000000008       O *COM*	0000000000000008 bufp1
0000000000000000 g     F .text	0000000000000042 swap
""")

main_c = Code(name='main.c', code="""
void swap();

int buf[2] = {1, 2};

int main() {
  swap();
  return 0;
}
""")

main_o = Code(name='main.o', code="""
0000000000000000 g     O .data	0000000000000008 buf
0000000000000000 g     F .text	000000000000001e main
0000000000000000         *UND*	0000000000000000 swap
""")


def generate() -> Document:
  tab = Table(['符号', 'swap.o .symtab 条目', '符号类型', '在哪个模块中定义', '节']) 
  tab.append('buf',    '是',                  '全局',     'main.o',           '.data')
  tab.append('bufp0',  '是',                  '全局',     'swap.o',           '.data')
  tab.append('bufp1',  '是',                  '局部',     'swap.o',           '*COM*')
  tab.append('swap',   '是',                  '全局',     'swap.o',           '.text')
  tab.append('temp',   '否')

  doc = Document()
  doc << main_c << main_o << swap_c << swap_o
  doc << tab
  return doc


if __name__ == '__main__':
    import sys
    generate().show(sys.stdout)


