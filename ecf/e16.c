#include <stdio.h>
#include <sys/wait.h>
#include <unistd.h>

int counter = 1;

int main() {
  if (fork() == 0) {
    counter--;  // 修改子进程的 counter
  } else {
    wait(NULL);
    printf("counter = %d\n",
           ++counter);  // 父进程的 counter 不受影响，此处输出 2
  }
  return 0;
}
