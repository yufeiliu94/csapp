#include <sys/wait.h>
#include <unistd.h>
#include <cstdio>
#include <cstdlib>

/**
 * 可能的输出序列
 * abcc
 * acbc
 * bacc
 * bcac // waitpid 时子进程未创立
 */
int main() {
  if (fork() == 0) {
    printf("a");
  } else {
    printf("b");
    waitpid(-1, nullptr, 0);
  }
  printf("c");
  std::exit(0);
}
