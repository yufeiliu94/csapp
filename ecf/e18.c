#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

void random_sleep() {
  int random_usec = rand() * (1000000000. / RAND_MAX);
  struct timespec m, q = {.tv_sec = 0, .tv_nsec = random_usec};
  nanosleep(&q, &m);
}

void end() { printf("2"); }

/**
 * -------- P1
 *   \   \- P0
 *    \---- P1P2
 *       \- P0P2
 * 因此不可能的序列是
 * 211020
 * 122001
 */
int main() {
  if (fork() == 0) {
    atexit(end);
  }
  if (fork() == 0) {
    random_sleep();
    printf("0");
  } else {
    random_sleep();
    printf("1");
  }
  random_sleep();
  return 0;
}
