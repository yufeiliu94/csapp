# encoding: UTF-8

from csapp.markdown import Code, Document
from os import linesep
from itertools import permutations

code = Code(name='e4.cpp', code=r'''
int main() {
  int status;
  printf("Hello\n");  // 进父进程输出一次
  const auto pid = fork();
  printf("%d\n", pid);  // 父进程和子进程各输出一次
  if (pid != 0) {
    if (waitpid(-1, &status, 0) > 0) {
      if (WIFEXITED(status) != 0) {
        printf("%d\n", WEXITSTATUS(status));  // 子进程结束后，父进程输出一次
      }
    }
  }
  printf("Bye\n");  // 父进程和子进程各输出一次
  exit(2);
}''')


def generate() -> Document:
    d = Document()
    d << code << linesep << '以上程序共产生6行输出，可能的输出序列是：' << linesep
    for seq in permutations(['0', '<PID>', 'Bye']):
        if seq.index('0') > seq.index('Bye'):
            continue
        d << 'Hello -> {} -> {} -> {} -> 2 -> Bye{}'.format(*seq, linesep)
    return d


if __name__ == '__main__':
    import sys
    generate().show(stream=sys.stdout)
