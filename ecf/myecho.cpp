#include <cstdio>

int main(int argc, char *argv[], char *envp[]) {
  printf("Command line arguments:\n");
  for (auto i = 0; i < argc; ++i) {
    printf("\targv[%3d]: %s\n", i, argv[i]);
  }
  printf("Environment variables:\n");
  for (auto i = 0; envp[i] != nullptr; ++i) {
    printf("\tenvp[%3d]: %s\n", i, envp[i]);
  }
}
