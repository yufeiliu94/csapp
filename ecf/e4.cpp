#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <unistd.h>

int main() {
  int status;
  printf("Hello\n");  // 进父进程输出一次
  const auto pid = fork();
  printf("%d\n", pid);  // 父进程和子进程各输出一次
  if (pid != 0) {
    if (waitpid(-1, &status, 0) > 0) {
      if (WIFEXITED(status) != 0) {
        printf("%d\n", WEXITSTATUS(status));  // 子进程结束后，父进程输出一次
      }
    }
  }
  printf("Bye\n");  // 父进程和子进程各输出一次
  exit(2);
}

/**
 * 以上程序共产生6行输出，一个可能的输出序列是：
 * Hello -> 0 -> PID -> Bye -> 2 -> Bye
 */
