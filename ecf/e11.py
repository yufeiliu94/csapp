# encoding: UTF-8

from csapp.markdown import Code, Document
from os import linesep


def generate() -> Document:
    return Document() << '共有 4 个进程被创造，打印 4 次 hello, 如下所示' << linesep << Code(code='''
P -- P --- P ---- hello
 \\  \\
  \\  ----- C1 --- hello
   \\
    -- C0 - C0 -- hello
        \\
         -- C2 -- hello''')


if __name__ == '__main__':
    import sys
    generate().show(stream=sys.stdout)

