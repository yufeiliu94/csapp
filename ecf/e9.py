# encoding: UTF-8

import sys
import itertools
from typing import Optional, Tuple
from csapp.markdown import Document, Table


class Process:
    def __init__(self, begin: int, end: int):
        self.begin, self.end = begin, end

    def union(self, p: 'Process') -> Optional[Tuple[int, int]]:
        if p.end <= self.begin or self.end <= p.begin:
            return None
        else:
            return max(p.begin, self.begin), min(p.end, self.end)


def generate() -> Document:
    ps = {}
    e1 = Document()
    tab_1 = Table(['进程', '起始时间', '结束时间'])
    tab_2 = Table(['进程对', '并发的', '重叠时段'])

    e1 << '考虑四个具有下述起始值和结束时间的进程：'     \
       << tab_1                                          \
       << '\n对于每个进程，指出他们是否时并发地运行的：' \
       << tab_2

    for name, start, end in (('A', 5, 7), ('B', 2, 4), ('C', 3, 5), ('D', 1, 8)):
        tab_1.append(name, start, end)
        ps[name] = Process(start, end)

    for lhs, rhs in itertools.combinations('ABCD', 2):
        l, r = ps[lhs], ps[rhs]
        u = l.union(r)
        tab_2.append(lhs + rhs, '是' if u is not None else '否', '' if u is None else str(u))

    return e1


if __name__ == '__main__':
    generate().show(sys.stdout)
