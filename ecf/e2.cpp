#include <unistd.h>
#include <cstdio>

int main() {
  int x = 1;
  if (fork() == 0) {
    printf("printf1: x = %d\n", ++x);  // 2
  } else {
    printf("printf2: x = %d\n", --x);  // 0
  }
  return 0;
}
