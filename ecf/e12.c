#include <stdio.h>
#include <unistd.h>

void doit() {
  fork();             // 1 -> 2
  fork();             // 2 -> 4
  printf("hello\n");  // x4
}

int main() {
  doit();
  printf("hello\n");  // x4
  return 0;
}
