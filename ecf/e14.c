#include <stdio.h>
#include <unistd.h>

/**
 * 共输出 6 次
 */
void doit() {
  if (fork() == 0) {  // 1 -> 2
    fork();           // 2 -> 3
  }
  printf("hello\n");  // x3
}

int main() {
  doit();
  printf("hello\n");  // x3
  return 0;
}
