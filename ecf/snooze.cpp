#include <sys/signal.h>
#include <unistd.h>
#include <cstdlib>
#include <iostream>
#include "csapp/error.hpp"

unsigned int snooze(unsigned int sec) {
  const auto rest = sleep(sec);
  const auto slept = sec - rest;
  std::cout << "Slept for " << slept << " of " << sec << " secs." << std::endl;
  return rest;
}

static bool interrupted = false;

void on_interrupt(int signal) { interrupted = signal == SIGINT; }

#if defined(WITH_MAIN)
int main(int argc, char *argv[]) {
  if (argc != 2) {
    std::cerr << "usage: snooze [N]" << std::endl;
    exit(-1);
  }
  csapp::UnixErrorHandler ue("[snooze] ");
  ue << (signal(SIGINT, on_interrupt) != SIG_ERR);
  const auto sec = std::atoi(argv[1]);
  const auto slept = sec - snooze(sec);
  if (interrupted) {
    std::cout << "User hits ctrl-c after " << slept << " seconds" << std::endl;
  }
  return 0;
}
#endif
