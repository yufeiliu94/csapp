#include <stdio.h>
#include <unistd.h>

/**
 * 输出 243 | 423 | 432
 */
int main() {
  int x = 3;
  if (fork() != 0) {
    printf("x = %d\n", ++x);
  }
  printf("x = %d\n", --x);
  return 0;
}
