#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

/**
 * 理想状态下，打印 max(2**n, 1) 次 hello
 * 实际状况下，不可能无限创建进程
 */
void foo(int n) {
  for (int i = 0; i < n; ++i) fork();
  printf("hello\n");
}

int main(int argc, char *argv[]) {
  if (argc != 2) {
    fprintf(stderr, "Usage: %s N | wc -l\n", argv[0]);
    return 1;
  }
  int n = atoi(argv[1]);
  n = n > 1 ? n : 1;
  foo(n);
  return 0;
}
