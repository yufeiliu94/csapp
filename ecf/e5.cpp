#include <cstdlib>
#include <iostream>

unsigned int snooze(unsigned int sec);

int main(int argc, char *argv[]) {
  if (argc != 2) {
    std::cerr << "usage: c8e5 [N]" << std::endl;
    exit(-1);
  }
  const auto sec = std::atoi(argv[1]);
  snooze(sec);
  return 0;
}
