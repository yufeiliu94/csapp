# encoding: UTF-8

from csapp.markdown import Code, Document
from os import linesep


def generate() -> Document:
    return Document() << '共有 4 个进程被创造，打印 8 次 hello, 如下所示' << linesep << Code(code='''
P -- P --- P ---- hello -- hello
 \\  \\
  \\  ----- C1 --- hello -- hello
   \\
    -- C0 - C0 -- hello -- helle
        \\
         -- C2 -- hello -- hello''')


if __name__ == '__main__':
    import sys
    generate().show(stream=sys.stdout)

