# encoding: UTF-8

import sys
import itertools
from typing import Optional, Tuple
from csapp.markdown import Document, Table


def generate() -> Document:
    ps = {}
    e10 = Document()
    tab = Table(['函数' , '行为'])
    tab.append('setjmp',  '调用一次，返回一次或多次')
    tab.append('longjmp', '调用一次，从不返回')
    tab.append('execve',  '调用一次，从不返回')
    tab.append('fork',    '调用一次，返回两次')
    return e10 << tab


if __name__ == '__main__':
    generate().show(sys.stdout)
