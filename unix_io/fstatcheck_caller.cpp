#include <unistd.h>
#include <sys/wait.h>
#include <iostream>
#include <fcntl.h>
#include "csapp/error.hpp"

int main(int argc, char *argv[]) {
  if (argc != 3) {
    std::cerr << "Usage: fstatcheck_caller [FSTATCHECK] [FILE]";
  }
  const auto pid = fork();
  if (pid == 0) {
    csapp::UnixErrorHandler e("[fstatcheck_caller] ");
    int fd;
    e.tie(fd) << open(argv[2], O_RDONLY, 0) << execl(argv[1], argv[1], "3", nullptr);
  }
  wait(nullptr);
  return 0;
}