project(UnixIO CXX)

# 10.1 -> 10.6
add_executable(unix_io_exercises exercises.cpp)
add_test(Chapter_10::Unix_IO unix_io_exercises)

# 10.7 10.10
add_executable(cpfile cpfile.cpp)

# 10.8
add_executable(fstatcheck fstatcheck.cpp)

# 10.9
add_executable(fstatcheck_caller fstatcheck_caller.cpp)
