#define CATCH_CONFIG_RUNNER
#include <catch.hpp>
#include <fcntl.h>
#include <fstream>
#include <sys/wait.h>
#include <unistd.h>

#define EXERCISE(chapter, exercise)                                            \
  TEST_CASE("Exercise " #exercise, "[Chapter " #chapter "]")

const auto foobar_txt = "foobar.txt";
const auto foobaz_txt = "foobaz.txt";

void setup() {
  auto os = std::ofstream(foobar_txt);
  os << "foobar";
  os.close();
  os = std::ofstream(foobaz_txt);
  os << "foobaz";
  os.close();
}

void cleanup() {
  std::remove(foobar_txt);
  std::remove(foobaz_txt);
}

EXERCISE(10, 1) {
  // 0-2 默认为 stdin, stdout, stderr, fd1 顺延为 3
  const auto fd1 = open(foobar_txt, O_RDONLY, 0);
  close(fd1);
  // 关闭后，文件描述符 fd1 被复用
  // 一般的，fd2 == 3，然而 ctest 会占用文件描述符 3
  const auto fd2 = open(foobar_txt, O_RDONLY, 0);
  REQUIRE(fd2 == fd1);
  close(fd2);
}

EXERCISE(10, 2) {
  char c;
  const auto fd1 = open(foobar_txt, O_RDONLY, 0);
  const auto fd2 = open(foobar_txt, O_RDONLY, 0);
  read(fd1, &c, 1);
  read(fd2, &c, 1);
  REQUIRE(c == 'f');
  close(fd1);
  close(fd2);
}

EXERCISE(10, 3) {
  char c;
  const auto fd = open(foobar_txt, O_RDONLY, 0);
  if (fork() == 0) {
    read(fd, &c, 1);
    _exit(0);
  }
  wait(nullptr);
  // 子进程共享打开文件表, c 将读到第 2 个字符 'o'
  read(fd, &c, 1);
  REQUIRE(c == 'o');
  close(fd);
}

EXERCISE(10, 4) {
  char buffer[6];
  const auto baz = open(foobaz_txt, O_RDONLY, 0);
  dup2(baz, baz + 1);
  // 复制的文件描述符表项应该对同一打开文件产生影响
  const auto six = read(baz + 1, buffer, 6);
  REQUIRE(six == 6);
  REQUIRE(buffer[5] == 'z');
  close(baz + 1);
  const auto zero = read(baz, buffer, 6);
  REQUIRE(zero == 0);
  REQUIRE(buffer[5] == 'z');
  close(baz);
}

EXERCISE(10, 5) {
  char c, buffer[6] = {};
  const auto fd1 = open(foobar_txt, O_RDONLY, 0);
  const auto fd2 = open(foobar_txt, O_RDONLY, 0);
  read(fd2, &c, 1);
  dup2(fd2, fd1);
  read(fd1, &c, 1);
  // 复制的文件描述符表项应该对同一打开文件产生影响
  REQUIRE(c == 'o');
  const auto four = read(fd1, buffer, 6);
  REQUIRE(four == 4);
  REQUIRE(std::string(buffer) == "obar");
  close(fd2);
}

EXERCISE(10, 6) {
  // 文件描述符复用
  const auto fd1 = open(foobar_txt, O_RDONLY, 0);
  const auto fd2 = open(foobaz_txt, O_RDONLY, 0);
  close(fd2);
  const auto fd3 = open(__FILE__, O_RDONLY, 0);
  REQUIRE(fd1 != fd2);
  REQUIRE(fd2 == fd3);
  close(fd3);
  close(fd1);
}

int main(int argc, char *argv[]) {
  setup();
  const auto ret = Catch::Session().run(argc, argv);
  cleanup();
  return ret;
}
