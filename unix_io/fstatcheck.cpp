#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <iostream>
#include "csapp.hpp"
#include "csapp/error.hpp"
#include "csapp/string.hpp"

int main(int argc, char *argv[]) {
  struct stat st;
  const char *type;
  csapp::UnixErrorHandler e("[fstatcheck] ");
  if (argc != 2) {
    std::cerr << "Usage: fstatcheck [FILENAME]" << std::endl;
    std::exit(-1);
  }
  const auto fd = csapp::string::stdlib_cast(std::strtod, argv[1]);
  if (fd.is_nothing()) {  // argv[1] 更可能是一个文件名
    e << stat(argv[1], &st);
  } else {
    e << fstat(*fd, &st);
  }
  if (S_ISREG(st.st_mode)) {
    type = "regular";
  } else if (S_ISDIR(st.st_mode)) {
    type = "directory";
  } else {
    type = "other";
  }
  const bool can_read = (st.st_mode & S_IRUSR) == S_IRUSR;
  const auto read = can_read ? "yes" : "no";
  std::cout << "type: " << type << ", read: " << read << std::endl;
  return 0;
}
