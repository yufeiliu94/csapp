#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include "csapp.hpp"
#include "csapp/error.hpp"
#include "csapp/rio.hpp"
#include "csapp/string.hpp"

static constexpr size_t buffer_size = 1024;

int main(int argc, char *argv[]) {
  ssize_t n;
  int infile;
  csapp::UnixErrorHandler e("[cpfile] ");
  char buffer[buffer_size];
  if (argc == 2) {
    e.tie(infile) = open(argv[1], O_RDONLY, 0);
    // 题目不允许在 9-12 之间插入新的代码; 使用 dup2 将 infile 重定向到 stdin
    dup2(infile, STDIN_FILENO);
  }
  csapp::rio::rio_t source(STDIN_FILENO);                 // 9
  while ((n = source.readn(buffer, buffer_size)) != 0) {  // 10
    csapp::rio::writen(STDOUT_FILENO, buffer, n);         // 11
  }                                                       // 12
  return 0;
}
